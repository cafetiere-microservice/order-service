package com.cafetiere.order.service;

import com.cafetiere.order.model.Order;
import com.cafetiere.order.repository.OrderRepository;
import com.cafetiere.order.vo.Account;
import com.cafetiere.order.vo.Product;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.web.client.RestTemplate;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertIterableEquals;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
@ExtendWith(MockitoExtension.class)
public class OrderServiceTest {

    @Mock
    private OrderRepository orderRepository;

    @InjectMocks
    private OrderServiceImpl orderService;

    @Mock
    private RestTemplate restTemplate;

    private Order order;

    private Account user;

    private Product product;

    @BeforeEach
    public void setUp() {
        user = new Account(1, "fname", "lname", "username@email.com", "username", "passwd");
        product = new Product(1, "pname", "pdescription", 15000, 1);

        LocalDateTime now = LocalDateTime.now();
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("EEE, d MMM yyyy HH:mm:ss");
        String formattedDateTime = now.format(formatter);
        LocalDateTime parsedDateTime = LocalDateTime.parse(formattedDateTime, formatter);

        order = new Order(product.getId(), 1);
        order.setId(1);
        order.setOrderTime(parsedDateTime);
        order.setUserId(user.getId());
        order.setSubtotal(product.getPrice() * order.getQuantity());
    }

    @Test
    public void testCreateOrderIsCorrectlyImplemented() {
        when(restTemplate.getForObject("http://PRODUCT-SERVICE/product/" + user.getId(), Product.class)).thenReturn(product);
        when(orderRepository.save(order)).thenReturn(order);
        Order newOrder = orderService.createOrder(order, user.getId());
        assertEquals(order.getId(), newOrder.getId());
        verify(orderRepository).save(order);
    }

    @Test
    public void testGetAllOrdersIsCorrectlyImplemented() {
        List<Order> orders = new ArrayList<>();
        orders.add(order);
        when(orderRepository.findAll()).thenReturn(orders);
        List<Order> result = orderService.getAllOrders(user.getId());
        assertIterableEquals(orders, result);
    }

    @Test
    public void testDeleteOrderIsCorrectlyImplemented() {
        when(restTemplate.getForObject("http://PRODUCT-SERVICE/product/" + user.getId(), Product.class)).thenReturn(product);
        orderService.createOrder(order, user.getId());
        orderService.deleteOrder(order.getId());
        assertEquals(null, orderService.getOrder(order.getId()));
        verify(orderRepository).deleteById(order.getId());
    }
}
