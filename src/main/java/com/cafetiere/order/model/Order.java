package com.cafetiere.order.model;

import com.cafetiere.order.state.CancelledState;
import com.cafetiere.order.state.ConfirmedState;
import com.cafetiere.order.state.OpenState;
import com.cafetiere.order.state.OrderState;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.*;
import org.hibernate.annotations.CreationTimestamp;

import javax.persistence.*;
import javax.validation.constraints.Positive;
import java.time.LocalDateTime;

@Entity
@Table(name = "order")
@Data
@NoArgsConstructor
public class Order {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @CreationTimestamp
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "EEE, d MMM yyyy HH:mm:ss")
    @Column(name = "order_time")
    private LocalDateTime orderTime;

    @Column(name = "user_id")
    private int userId;

    @Column(name = "product_id")
    private int productId;

    @Positive(message = "Quantity must be a positive value")
    private int quantity;

    private int subtotal;

    @Transient
    @Setter(AccessLevel.NONE)
    private OrderState openState;

    @Transient
    @Setter(AccessLevel.NONE)
    private OrderState confirmedState;

    @Transient
    @Setter(AccessLevel.NONE)
    private OrderState cancelledState;

    @Column(name = "status")
    private OrderState orderState;

    public Order(int productId, int quantity) {
        this.productId = productId;
        this.quantity = quantity;

        openState = new OpenState(this);
        confirmedState = new ConfirmedState(this);
        cancelledState = new CancelledState(this);

        orderState = openState;
    }

    public void confirmOrder() {
        orderState.confirmOrder();
    }

    public void cancelOrder() {
        orderState.cancelOrder();
    }

    public String getStateDescription() {
        return orderState.getStateDescription();
    }

    public boolean isFinished() {
        return orderState.isFinished();
    }
}
