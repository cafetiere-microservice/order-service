package com.cafetiere.order.deserializer;

import com.cafetiere.order.state.CancelledState;
import com.cafetiere.order.state.ConfirmedState;
import com.cafetiere.order.state.OpenState;
import com.cafetiere.order.state.OrderState;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;

import java.io.IOException;

public class OrderStateDeserializer extends JsonDeserializer<OrderState> {

    @Override
    public OrderState deserialize(JsonParser jp, DeserializationContext ctxt) throws IOException, JsonProcessingException {
        ObjectMapper mapper = (ObjectMapper) jp.getCodec();
        ObjectNode root = mapper.readTree(jp);
        Class<? extends OrderState> instanceClass = null;



        if () {
            instanceClass = OpenState.class;
        } else if () {
            instanceClass = ConfirmedState.class;
        } else {
            instanceClass = CancelledState.class;
        }

        if (instanceClass == null){
            return null;
        } else {
            return mapper.convertValue(root, instanceClass);
        }
    }
}
