package com.cafetiere.order.controller;

import com.cafetiere.order.model.Order;
import com.cafetiere.order.service.OrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(path = "/order")
public class OrderController {

    @Autowired
    private OrderService orderService;

    @PostMapping(path = "/{userId}", produces = { "application/json" })
    @ResponseBody
    public ResponseEntity<Order> createOrder(@RequestBody Order order, @PathVariable(value = "userId") int userId) {
        Order response = orderService.createOrder(order, userId);
        return ResponseEntity.ok(response);
    }

    @GetMapping(path = "/{userId}", produces = { "application/json" })
    @ResponseBody
    public ResponseEntity<List<Order>> getAllOrders(@PathVariable(value = "userId") int userId) {
        List<Order> response = orderService.getAllOrders(userId);
        return ResponseEntity.ok(response);
    }

    @DeleteMapping(path = "/{orderId}", produces = { "application/json" })
    public ResponseEntity<Order> deleteOrder(@PathVariable(value = "orderId") int orderId) {
        orderService.deleteOrder(orderId);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }
}
