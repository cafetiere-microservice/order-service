package com.cafetiere.order.service;

import com.cafetiere.order.model.Order;

import java.util.List;

public interface OrderService {

    Order createOrder(Order order, int userId);

    Order getOrder(int orderId);

    List<Order> getAllOrders(int userId);

    int getTotal(int userId);

    void deleteOrder(int orderId);
}
