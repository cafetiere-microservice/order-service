package com.cafetiere.order.service;

import com.cafetiere.order.model.Order;
import com.cafetiere.order.repository.OrderRepository;
import com.cafetiere.order.vo.Account;
import com.cafetiere.order.vo.Product;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.List;

@Service
public class OrderServiceImpl implements OrderService {

    @Autowired
    private OrderRepository orderRepository;

    @Autowired
    private RestTemplate restTemplate;

    @Override
    public Order createOrder(Order order, int userId) {
        Product product = restTemplate.getForObject("http://PRODUCT-SERVICE/product/" + order.getProductId(), Product.class);
        int subtotal = product.getPrice() * order.getQuantity();
        order.setSubtotal(subtotal);
        order.setUserId(userId);
        orderRepository.save(order);
        return order;
    }

    @Override
    public Order getOrder(int orderId) {
        return orderRepository.getById(orderId);
    }

    @Override
    public List<Order> getAllOrders(int userId) {
        Account user = restTemplate.getForObject("http://AUTH-SERVICE/user/" + userId, Account.class);
        String username = user.getUsername();
        List<Order> orders = orderRepository.findAll();
        if (!username.equalsIgnoreCase("admin")) {
            orders.removeIf(order -> order.getUserId() != userId);
        }
        return orders;
    }

    @Override
    public void deleteOrder(int orderId) {
        orderRepository.deleteById(orderId);
    }

    @Override
    public int getTotal(int userId) {
        int total = 0;
        List<Order> orders = getAllOrders(userId);
        for (Order order : orders) {
            total += order.getSubtotal();
        }
        return total;
    }
}
