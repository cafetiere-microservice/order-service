package com.cafetiere.order.state;

public interface OrderState {

    String getStateDescription();

    void confirmOrder();

    void cancelOrder();

    boolean isFinished();
}
