package com.cafetiere.order.state;

import com.cafetiere.order.model.Order;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;

@JsonDeserialize(as = CancelledState.class)
public class CancelledState implements OrderState {
    Order order;

    public CancelledState(Order order) {
        this.order = order;
    }

    @Override
    public String getStateDescription() {
        return "Cancelled";
    }

    @Override
    public void confirmOrder() {
        throw new IllegalStateException(
                "Can't cancel an order when the order is in" + getStateDescription() + " state."
        );
    }

    @Override
    public void cancelOrder() {
        throw new IllegalStateException(
                "Can't cancel an order when the order is in " + getStateDescription() + " state."
        );
    }

    @Override
    public boolean isFinished() {
        return true;
    }

    @Override
    public String toString() {
        return "Cancelled / Expired";
    }
}
