package com.cafetiere.order.state;

import com.cafetiere.order.model.Order;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;

@JsonDeserialize(as = OpenState.class)
public class OpenState implements OrderState {

    Order order;

    public OpenState(Order order) {
        this.order = order;
    }

    @Override
    public String getStateDescription() {
        return "Open";
    }

    @Override
    public void confirmOrder() {
        OrderState confirmedState = order.getConfirmedState();
        order.setOrderState(confirmedState);
    }

    @Override
    public void cancelOrder() {
        OrderState cancelledState = order.getCancelledState();
        order.setOrderState(cancelledState);
    }

    @Override
    public boolean isFinished() {
        return false;
    }

    @Override
    public String toString() {
        return "Waiting for payment";
    }
}
