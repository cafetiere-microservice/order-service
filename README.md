# Cafetiere Application

## Status
[![coverage report](https://gitlab.com/cafetiere-microservice/order-service/badges/master/coverage.svg)](https://gitlab.com/cafetiere-microservice/order-service/-/commits/master)
[![pipeline status](https://gitlab.com/cafetiere-microservice/order-service/badges/master/pipeline.svg)](https://gitlab.com/cafetiere-microservice/order-service/-/commits/master)


## Group Member
Piawai Said Umbara

## Feature
Buy Product

## Production Notes
- [Database Schema](http://bit.ly/cafetiereSchema)
- [Deployed Website](https://cafetiere-backend.herokuapp.com)

## Project Structure
- **Controller**: Put all your endpoint controller here.
- **Model**: Entity class for your database.
- **Repository**: Creating database query.
- **Service**: Business logic code is here.